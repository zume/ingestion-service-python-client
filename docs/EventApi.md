# swagger_client.EventApi

All URIs are relative to *https://dev.data.zume-k8s.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ingest_event**](EventApi.md#ingest_event) | **POST** /v1/event | ingest new events list
[**ingest_event_v2**](EventApi.md#ingest_event_v2) | **POST** /v2/event | ingest new event V2 list

# **ingest_event**
> ingest_event(body)

ingest new events list

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.EventApi(swagger_client.ApiClient(configuration))
body = [swagger_client.IngestionEvent()] # list[IngestionEvent] | Endpoint for event ingestion

try:
    # ingest new events list
    api_instance.ingest_event(body)
except ApiException as e:
    print("Exception when calling EventApi->ingest_event: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[IngestionEvent]**](IngestionEvent.md)| Endpoint for event ingestion | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ingest_event_v2**
> ingest_event_v2(body)

ingest new event V2 list

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.EventApi(swagger_client.ApiClient(configuration))
body = [swagger_client.IngestionEventV2()] # list[IngestionEventV2] | Endpoint for eventV2 ingestion

try:
    # ingest new event V2 list
    api_instance.ingest_event_v2(body)
except ApiException as e:
    print("Exception when calling EventApi->ingest_event_v2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[IngestionEventV2]**](IngestionEventV2.md)| Endpoint for eventV2 ingestion | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

