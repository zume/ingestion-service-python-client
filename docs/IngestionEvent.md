# IngestionEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**version** | **str** |  | 
**creation_timestamp** | **int** |  | [optional] 
**environment** | **str** |  | 
**name** | **str** |  | 
**tenant_id** | **str** |  | 
**correlationid** | **str** |  | [optional] 
**payload_type** | **str** |  | 
**payload_version** | **int** |  | [optional] 
**payload** | **object** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

