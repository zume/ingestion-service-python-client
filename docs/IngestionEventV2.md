# IngestionEventV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**envelope_version** | **str** |  | 
**created** | **str** |  | 
**environment** | **str** |  | 
**platform** | **str** |  | 
**source** | **str** |  | 
**tenant_id** | **str** |  | 
**event_type** | **str** |  | 
**event_version** | **str** |  | 
**event_data** | **object** |  | 
**device_id** | **str** |  | [optional] 
**app_name** | **str** |  | [optional] 
**app_version** | **str** |  | [optional] 
**user_id** | **str** |  | [optional] 
**correlation_id** | **str** |  | [optional] 
**metadata** | **object** |  | [optional] 
**custom** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

